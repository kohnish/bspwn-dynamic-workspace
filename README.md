Usage: 

    In sxhkdrc:
        ctrl + shift + f
            bspwm-dynamic-workspace

    In bspwmrc:
        bspc subscribe node_add node_remove | while read line; do bspwm-dynamic-workspace keep_minimum_empty_workspaces; done &
