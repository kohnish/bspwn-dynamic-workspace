#!/bin/sh

declare -A empty_workspaces
empty_workspace=""

function get_empty_workspaces {
    local workspace
    empty_workspaces=()
    for workspace in `bspc query --desktops`; do
        if [[ $(bspc query --nodes --desktop $workspace | fgrep -v "$(bspc query --desktop $workspace -N -n .hidden)" |wc -l) -eq 0 ]]; then
            empty_workspaces[${#empty_workspaces[@]}]=$workspace
        fi
    done
}

function keep_minimum_empty_workspaces {
    get_empty_workspaces
    while [[ ${#empty_workspaces[@]} < 1 ]]; do
        bspc monitor --add-desktops ''
        get_empty_workspaces
    done

    if [[ `bspc query --desktops | wc -l` > 2 ]]; then
        while [[ ${#empty_workspaces[@]} > 1 ]]; do
            while [[ $(bspc query --nodes --desktop `bspc query --desktops --desktop` | wc -l) > 1 ]]; do
                bspc desktop --focus next
            done
            bspc desktop --remove
            get_empty_workspaces
        done
    fi
}

function find_empty_workspace {
    local workspace
    for workspace in `bspc query --desktops`; do
        if [[ `bspc query --nodes --desktop $workspace |wc -l` -eq 0 ]]; then
            empty_workspace=$workspace
            break
        fi
    done
}

function move_to_next_workspace {
    local current_window
    local window_num
    keep_minimum_empty_workspaces
    current_window=`bspc query --nodes --node`
    window_num=$(bspc query --nodes --desktop `bspc query --desktops --desktop` |wc -l)
    if [[ $window_num == 0 ]]; then
        :
    elif [[  $window_num == 1 ]]; then
        first_workspace=`bspc query --desktops|head -n1`
        bspc node $current_window --to-desktop $first_workspace
        keep_minimum_empty_workspaces
        bspc node --focus $current_window
    else
        find_empty_workspace
        bspc node --to-desktop $empty_workspace
        keep_minimum_empty_workspaces
        bspc node --focus $current_window
    fi
}

if [[ $1 == "keep_minimum_empty_workspaces" ]]; then
    keep_minimum_empty_workspaces
else
    move_to_next_workspace
fi
